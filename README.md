## Description

Nest is a framework for building efficient, scalable <a href="http://nodejs.org" target="_blank">Node.js</a> server-side applications. It uses modern JavaScript, is built with  <a href="http://www.typescriptlang.org" target="_blank">TypeScript</a> (preserves compatibility with pure JavaScript) and combines elements of OOP (Object Oriented Programming), FP (Functional Programming), and FRP (Functional Reactive Programming).

<p>Under the hood, Nest makes use of <a href="https://expressjs.com/" target="_blank">Express</a>, but also, provides compatibility with a wide range of other libraries, like e.g. <a href="https://github.com/fastify/fastify" target="_blank">Fastify</a>, allowing for easy use of the myriad third-party plugins which are available.</p>

## Running The Application

2. For running the application on local machine: `cp .env.example .env`
3. Installing dependencies: `npm install`
4. Start the server: `npm start`
5. Run integration test: `npm run test:e2e`

## Rest API :

#### Get Trailer Link

This API will return the trailer link of given video.

- **method** : `GET`

- **url**: `/trailers`

- **Request Params (querystring)**:
    - link: `Viaplay resource link (string)`
