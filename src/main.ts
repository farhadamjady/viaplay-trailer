import { NestFactory }         from '@nestjs/core';
import { AppModule }           from './app.module';
import { AllExceptionsFilter } from './common/exceptions/all-exceptions.filter';
import { configService }       from './config.service';
import { ResponseDecorator }   from './common/response-decorator/responseDecorator.interceptor';
import { ValidationPipe }      from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalFilters(new AllExceptionsFilter());

  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  // Decorate all responses
  app.useGlobalInterceptors(new ResponseDecorator());

  await app.listen(3000);
}
bootstrap();
