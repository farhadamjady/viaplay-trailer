import { CallHandler, ExecutionContext, HttpStatus, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable }                                                             from 'rxjs';
import { map }                                                                    from 'rxjs/operators';
import { Request }                                                                from 'express';
import { ControllerResponse, Response }                                           from './responses.interface';
import { configService }                                                          from '../../config.service';

@Injectable()
export class ResponseDecorator<T> implements NestInterceptor<T, Promise<Response>> {

  /**
   *
   * @param context
   * @param next
   */
  intercept(context: ExecutionContext, next: CallHandler): Observable<Promise<unknown>> {
    return next.handle().pipe(map(async (controllerResponse: ControllerResponse): Promise<void> => {

        // Extract request and response from the context
        const request = context.switchToHttp().getRequest();
        const response = context.switchToHttp().getResponse();

        // Extract some values from controller's response
        const { data, total } = controllerResponse;

        // Set the http-status
        const httpStatus = controllerResponse.httpStatus || HttpStatus.OK;

        // Init the status
        const status = (httpStatus >= 200 && httpStatus < 400) ? 'success' : 'fail';

        // Init the message
        const message = controllerResponse.message || 'OK';

        // Modify the response
        response.statusCode = httpStatus;
        response.json({
          status,
          message,
          data
        });
      }
    ));
  }
}
