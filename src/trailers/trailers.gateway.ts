const axios = require('axios');

export class TrailersGateway {

  /**
   *
   * @param link
   */
  public async getData(link: string) {
    const response = await axios.get(link);
    return response.data;
  }
}
