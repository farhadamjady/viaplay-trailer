import { Controller, Get, Query, Req, Res } from '@nestjs/common';
import { TrailersService }                  from './trailers.service';
import { Request }                          from 'express';
import { ControllerResponse }               from '../common/response-decorator/responses.interface';
import GetTrailerDto                        from './dto/get-trailer.dto';

@Controller('trailers')
export class TrailersController {

  constructor(private readonly trailersService: TrailersService) {}

  /**
   *
   * @param getTrailerDto
   */
  @Get()
  public async getTrailer(@Query() getTrailerDto: GetTrailerDto): Promise<ControllerResponse> {
    const resourceLink = getTrailerDto.link;
    const trailerLink = await this.trailersService.getTrailer(resourceLink);
    return { data: { trailerLink } };
  }
}
