import { IsString } from 'class-validator';

export default class GetTrailerDto {
  @IsString()
  link: string;
}
