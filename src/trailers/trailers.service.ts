import { Injectable }        from '@nestjs/common';
import { ConfigService }     from '@nestjs/config';
import { TheMovieDBService } from './themoviedb/themoviedb.service';
import { TrailersGateway }   from './trailers.gateway';

@Injectable()
export class TrailersService {

  protected trailerAPIService;
  protected trailerGateway;

  constructor(
    private readonly configService: ConfigService
  ) {
    this.trailerGateway = new TrailersGateway();
    switch (configService.get<string>('TRAILER_API_SERVICE')) {
      case 'THE_MOVIE_DB':
        this.trailerAPIService = new TheMovieDBService();
        break;
      default:
        this.trailerAPIService = new TheMovieDBService();
        break;
    }
  }

  /**
   *
   * @param link
   */
  public async getTrailer(link: string) {
    const resourceData = await this.getDataByLink(link);
    return await this.trailerAPIService.getTrailer(resourceData);
  }

  /**
   *
   * @param link
   * @private
   */
  private async getDataByLink(link: string) {
    return await this.trailerGateway.getData(link);
  }
}
