import { configService } from '../../config.service';
const axios = require('axios');

export class TheMovieDBGateway {

  protected baseURL;
  protected APIKey;

  constructor() {
    this.baseURL = configService.get<string>('THE_MOVIE_DB_BASE_URL');
    this.APIKey = configService.get<string>('THE_MOVIE_DB_API_KEY');
  }

  /**
   *
   * @param IMDBId
   */
  public async getTrailer(IMDBId: string) {
    const response = await axios.get(`${this.baseURL}/${IMDBId}/videos?api_key=${this.APIKey}`);
    return response.data.results;
  }
}
