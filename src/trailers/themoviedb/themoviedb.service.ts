import { TheMovieDBGateway }            from "./themoviedb.gateway";
import { InternalServerErrorException } from '@nestjs/common';
import { configService }                from '../../config.service';

export class TheMovieDBService {

  protected gateway;

  constructor() {
    this.gateway = new TheMovieDBGateway();
  }

  /**
   *
   * @param data
   */
  public async getTrailer(data: any):  Promise<string> {
    const IMDBData = data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb;
    const IMDBId = IMDBData.id;
    const videoResults = await this.gateway.getTrailer(IMDBId);
    if (videoResults && videoResults.length) {
      for (const videoResult of videoResults) {
        if (videoResult.type === 'Trailer') {
          return `${configService.get<string>('YOUTUBE_BASE_URL')}watch?v=${videoResult.key}`;
        }
      }
    }
    throw new InternalServerErrorException('No Trailer Found For This Movie');
  }
}
