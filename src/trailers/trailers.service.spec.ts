import { Test, TestingModule } from '@nestjs/testing';
import { TrailersService }     from './trailers.service';
import { ConfigModule } from '@nestjs/config';
import { TrailersController } from './trailers.controller';

describe('TrailersService', () => {
  let service: TrailersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule
      ],
      controllers: [TrailersController],
      providers: [TrailersService]
    }).compile();

    service = module.get<TrailersService>(TrailersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
