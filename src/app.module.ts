import { Module }         from '@nestjs/common';
import { TrailersModule } from './trailers/trailers.module';

@Module({
  imports: [TrailersModule]
})
export class AppModule {}
