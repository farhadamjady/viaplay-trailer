import { Test, TestingModule }              from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request                         from 'supertest';
import { AppModule }                        from './../src/app.module';
import { AllExceptionsFilter }              from '../src/common/exceptions/all-exceptions.filter';
import { ResponseDecorator }                from '../src/common/response-decorator/responseDecorator.interceptor';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalFilters(new AllExceptionsFilter());

    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    // Decorate all responses
    app.useGlobalInterceptors(new ResponseDecorator());

    await app.init();
  });

  it('/ (GET) Single trailer', async () => {
    const res = await request(app.getHttpServer())
      .get('/trailers?link=https://content.viaplay.se/pc-se/film/arrival-2016');
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('trailerLink');
  });

  it('/ (GET) Single trailer with error', async () => {
    const res = await request(app.getHttpServer())
      .get('/trailers?link=https://content.viaplay.se/pc-se/film/arrival-201656655');
    expect(res.status).toEqual(500);
    expect(res.body.message).toEqual('Request failed with status code 404');
  });

  it('/ (GET) Single trailer with validation error', async () => {
    const res = await request(app.getHttpServer())
      .get('/trailers?link2=https://content.viaplay.se/pc-se/film/arrival-2016');
    expect(res.status).toEqual(400);
    expect(res.body.message).toEqual('Bad Request Exception');
  });
});
